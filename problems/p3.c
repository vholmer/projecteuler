// Written by Viktor Holmér 2015

// Link to the problem: https://projecteuler.net/problem=3

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

void primeFactors(unsigned long n) {
	unsigned long orgNum = n;
	while(n % 2 == 0) { //If divisble by 2 then 2 is a prime factor
		printf("2 * ");
		n /= 2;
	}
	unsigned long i;
	for(i = 3; i <= sqrt(n); i += 2) {
		while(n % i == 0) { //If divisible by increments of i 
			printf("%lu * ", i); //Print i as prime factor
			n /= i;
		}
	}
	if(n > 2) {
		printf("%lu ", n);
	}
	printf("= %lu\n", orgNum);
}


int main(void) {
	unsigned long input;
	printf("Enter number to prime-factorize: ");
	scanf("%lu", &input);
	primeFactors(input);
	return 0;
}