import java.util.Scanner;

public class p6 {
	
	private static int sumSquare(int n) {
		int ret = 0;
		for(int i = 1; i <= n; ++i) {
			ret += i*i;
		}
		return ret;
	}
	
	private static int squareSum(int n) {
		int ret = 0;
		for(int i = 1; i <= n; ++i) {
			ret += i;
		}
		ret *= ret;
		return ret;
	}
	
	public static int diff(int n) {
		return squareSum(n) - sumSquare(n);
	}
	
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter n: ");
		int n = scan.nextInt();
		System.out.println("Sum square difference with n = " + n + " is: " + diff(n));
		scan.close();
	}
}
