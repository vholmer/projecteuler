// Written by Viktor Holmér 2015

// Link to the problem: https://projecteuler.net/problem=1

#include <stdio.h>

// Calculates and returns the value of
// all 3/5 multiples below the count parameter
// value recursively. Not effective for
// large values on count.
int calculate(int count, int startvalue) {
	int i;
	int a = startvalue; // Value to start counting from
	for(i = a; i < count; i++) {
		if(i % 3 == 0 || i % 5 == 0) {
			return i + calculate(count, a + 1); // Start summing up each 3/5-multiple
		}
		else {
			a++; // If not 3/5-multiple, increase a
		}
	}
	return 0;
}

int main(void) {
	int count = 1000;
	printf("The sum of all 3/5-multiples below %d is: %d\n", count, calculate(count, 0));
	return 0;
}