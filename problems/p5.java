public class p5 {
	
	public static int calculate(int range) {
		boolean foundNum = false;
		int res = 1;
		for(; foundNum == false; ++res) {
			for(int i = 1; i <= range; ++i) {
				if(res % i != 0) {
					break;
				} else if(i == range) {
					foundNum = true;
					res--;
				}
			}
		}
		return res;
	}
	
	public static void main(String[] args) {
		System.out.println(calculate(20));
	}
}