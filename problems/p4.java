import java.util.Scanner;

public class p4 {
	
	private int numDigits;
	
	p4(int numDigits) {
		this.numDigits = numDigits;
	}
	
	private boolean isPalindrome(long x) {
		String str = String.valueOf(x);
		for(int i = 0; i < str.length(); ++i) {
			if(str.charAt(i) != str.charAt(str.length() - 1 - i)) {
				return false;
			}
		}
		return true;
	}
	
	public long calculatePalindrome() {
		int lowLimit = (int)Math.pow(10, this.numDigits - 1);
		int highLimit = (int)Math.pow(10, this.numDigits);
		
		long maxFound = 0;
		int n = 0;
		
		for(int i = lowLimit; i < highLimit; ++i) {
			for(int j = lowLimit; j < highLimit; ++j) {
				long possPalin = i * j;
				if(isPalindrome(possPalin)) {
					if(possPalin > maxFound) {
						maxFound = possPalin;
						n++;
						System.out.println("#" + n + ": " + i + " * " + j + " = " + maxFound);
					}
				}
			}
		}
		
		return maxFound;
	}
	
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.print("Enter number of digits of x * y = palindrome: ");
		System.out.println();
		int nDigits = scan.nextInt();
		scan.close();
		p4 obj = new p4(nDigits);
		System.out.println(obj.calculatePalindrome() +
				" is the biggest palindrome that is a multiple of two " +
				obj.numDigits + "-digit numbers.");
	}
}