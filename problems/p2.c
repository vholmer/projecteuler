// Written by Viktor Holmér 2015

// Link to the problem: https://projecteuler.net/problem=2

#include <stdio.h>
#include <stdlib.h>

// Calculates the sum of all fib numbers
// below (non-inclusive) the parameter num.
int calculate(int num) {
	int first = 0, second = 1, current = 1, sum = 0;

	while (current < num) {

		if (current % 2 == 0) {
			sum += current;
		}

		first = second;
		second = current;
		current = first + second;
	}

	return sum;
}

int main(void) {
	int num;
	printf("Please input an integer value > 0: ");
	scanf("%d", &num);

	if(num < 0) {
		printf("Invalid input.\n");
		exit(1);
	}

	printf("Sum of even-valued fibs < %d: %d\n", num, calculate(num));
	return 0;
}
