import java.util.Scanner;

public class p7 {
	
	private static boolean isPrime(double currPrime) {
		boolean retBool = true;
		if(currPrime <= 1) {
			return false;
		}
		for(int i = 2; i <= Math.sqrt(currPrime); ++i) {
			if(currPrime % i == 0) {
				retBool = false;
				break;
			}
		}
		return retBool;
	}
	
	private static double getNextPrime(double inputPrime) {
		boolean foundPrime = false;
		double currPrime = inputPrime;
		while(!foundPrime) {
			currPrime++;
			if(isPrime(currPrime))
				foundPrime = true;
		}
		return currPrime;
	}
	
	// Returns prime #n.
	private static int getPrime(int n) {
		int primeCount = 1;
		double currPrime = 2;
		while(primeCount != n) {
			primeCount++;
			currPrime = getNextPrime(currPrime);
		}
		return (int)currPrime;
	}
	
	public static void main(String[] args) throws InterruptedException {
		Scanner scan = new Scanner(System.in);
		System.out.println("Which prime do you want: ");
		int input = scan.nextInt();
		System.out.println("Prime #" + input + " = " + getPrime(input));
		scan.close();
	}
}